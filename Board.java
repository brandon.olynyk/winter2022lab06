public class Board
{
	private Die die1;
	private Die die2;

	boolean[] closedTiles;

	
	public Board()
	{
		die1 = new Die();
		die2 = new Die();
		this.closedTiles = new boolean[12];
	}

	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();

		System.out.println(this.die1);
		System.out.println(this.die2);

		int pipSum = this.die1.getPips() + this.die2.getPips();

		if (this.closedTiles[pipSum - 1])
		{
			//if tile of pipsum already closed, say so and return true.
			System.out.println("The tile #" + pipSum + " is already closed.");
			return true;
		}
		else
		{
			//if not, close it, and return false.
			this.closedTiles[pipSum - 1] = true;
			System.out.println("Closing tile " + pipSum);
			return false;
		}
	}

	public String toString()
	{	
		String builder = "";

			for (int i=0; i < this.closedTiles.length; i++)
			{
				//builder += "Tile " + (i+1) + ": " + this.closedTiles[i] + "\n";
				if (this.closedTiles[i])
				{
					//puts x when number is closed
					builder += "X";
				}
				else
				{
					//prints number when tile is open
					builder += (i+1);
				}
				builder += " "; //Add a space for readability's sake
			}
		return builder;
	}
}