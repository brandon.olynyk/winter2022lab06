import java.util.Scanner;
public class ShutTheBox
{
	public static void main (String[] args)
	{	
		//initialize vars
		boolean gameOver = false;	//bool to manage the loop for each game
		boolean programOver = false; //bool to manage the loop for the program
		
		int player1Score = 0;
		int player2Score = 0;

		Scanner scan = new Scanner(System.in); //scanner for end of game

		//start program loop
		while(!programOver)
		{
			Board board = new Board();
			System.out.println("~WELCOME TO THE [SHUT THE BOX] EXPERIENCE~");
			while(!gameOver)
			{
				//main game loop
				//player1's turn
				System.out.println("Player 1's turn.");
				System.out.println(board);
				if(board.playATurn())
				{
					//game over, player 2 wins
					System.out.println("Player 2 wins!");
					player2Score++;
					gameOver = true;
				}
				else
				{
					//player 2's turn
					System.out.println("Player 2's turn.");
					System.out.println(board);
	
					if(board.playATurn())
					{
						System.out.println("Player 1 wins!");
						player1Score++;
						gameOver = true;
					}
					//else, redo the game loop
				}
			}
			//ask for another round and print scores
			System.out.println("Player 1's score: " + player1Score);
			System.out.println("Player 2's score: " + player2Score);
			
			System.out.println("Try your luck at another round?");
			System.out.println("Enter \"Y\" or \"N\" to make your choice. (Any unknown value will quit the game)");
			String response = scan.next().toLowerCase();
			if(response.equals("y")){
				//stop game loop if user says anything but yes
				programOver = false;
				gameOver = false;
			}
			else{
				programOver = true;
			}
		}
		
	}
}